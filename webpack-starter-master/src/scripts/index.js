import '../styles/index.scss';
// import { read } from 'fs';

//dette kan gjøres med getElementByTagname("button");
// forloot med const buttons som blir liste = getElementByTagname
console.log('webpack starterkit');
var currNum = 0; // calculated num
var userInput = ''; // input from user
var lastIsOpr = false;
var inParent = false;

const buttons = document.getElementsByTagName("button");

for(let i = 0; i < buttons.length; i++){
    buttons[i].addEventListener('click', function(){
        switch(buttons[i].value){
            case '+':
                // addition
                if(lastIsOpr){
                    userInput = userInput.slice(0,-1);
                 }
                userInput += '+';
                lastIsOpr = true;
                break;
            case '-': 
                //subtraction
                if(lastIsOpr){
                    userInput = userInput.slice(0,-1);
                 }
                userInput += '-';
                lastIsOpr = true;
                break;
            case '*': 
                // multiply
                if(lastIsOpr){
                    userInput = userInput.slice(0,-1);
                 }
                userInput += '*';
                lastIsOpr = true;
                break;
            case '/': 
                // division
                if(lastIsOpr){
                    userInput = userInput.slice(0,-1);
                 }
                userInput += '/';
                lastIsOpr = true;
                break;
            case '=':
                clearOutput();
                userInput === '' ? addOutput(0) : addOutput(eval(userInput));
                clearAll();
                break;
            case 'c':
                clearAll();
                // clear output as well
                clearOutput();
                break;
            case '(':
                userInput += '(';
                inParent = true;
                break;
            case ')':
                userInput += ')';
                inParent = false;
                break;
            default:
                userInput += `${buttons[i].value}`;
                if(!inParent){
                    addPreemtiveCal(userInput);
                }
                lastIsOpr = false;
                break;
            }
        if(buttons[i].value !== '=' && buttons[i].value !== 'c'){ // not working for c 
            addOutput(userInput);
        }
    });
}


function clearAll(){
    currNum = 0;
    userInput = '';
}

const addOutput = num => document.getElementById('input').value = num;
const addPreemtiveCal = num => document.getElementById('preemtiveCal').value = eval(userInput);
const clearOutput = () => {
    document.getElementById('input').value = 0;
    document.getElementById('preemtiveCal').value = 0;
};